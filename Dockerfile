FROM python:3
COPY requirements.txt /tmp/
#RUN pip install -r /tmp/requirements.txt
RUN pip install --proxy=http://10.55.123.98:3128 -r /tmp/requirements.txt

RUN mkdir -p /app
WORKDIR /app/src
COPY . /app/

CMD ["python", "-u", "./grid_calculation.py"]
