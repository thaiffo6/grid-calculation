import configparser
from pymongo import MongoClient
import folium
from itertools import chain
from folium.plugins import MarkerCluster
from statistics import mean
import json
import time
import numpy as np
import schedule
import logging
import traceback
np.warnings.filterwarnings('ignore')

from utils import find_geojson_bounding_box, remove_dupplicate_records, find_container_grid
from utils import get_geojson_grid, find_geojson_limit, check_inside_geojson
from map_grid import MapGrid

class GridCalculation():
    def __init__(self, config_path):
        self.config_logger()
        self.logger = logging.getLogger('real-estate-data-process.grid_calculation')
        self.config = configparser.RawConfigParser()
        self.config.read(config_path)
        self.record_type = self.config.get('data_type', 'record_type')
        self.stride = float(self.config.get('grid_method', 'stride'))
        self.min_ppm2 = float(self.config.get('price_limit', 'min_ppm2'))
        self.max_ppm2 = float(self.config.get('price_limit', 'max_ppm2'))
        vnm_geojson_path = self.config.get('process_record', 'vnm_geojson_file')
        with open(vnm_geojson_path) as f:
            self.vnm_geojson_data = json.load(f)
        self.administrative_limit_level = int(self.config.get('area_limit', 'administrative_limit_level'))
        self.administrative_limit_id = self.config.get('area_limit', 'administrative_limit_id')
        self.limit_geojson = find_geojson_limit(self.vnm_geojson_data, self.administrative_limit_level, self.administrative_limit_id)

        vnm_geojson_bounding_box = find_geojson_bounding_box(self.limit_geojson)
        self.lower_left_limit = [vnm_geojson_bounding_box[1], vnm_geojson_bounding_box[0]]
        self.upper_right_limit = [vnm_geojson_bounding_box[3], vnm_geojson_bounding_box[2]]

    @staticmethod
    def config_logger():
        logger = logging.getLogger("real-estate-data-process")
        logger.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('%(asctime)s [%(levelname)-5s] '
                                      '%(filename)s(%(lineno)-3s) %(message)s')
        # Log to console
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    def read_database(self):
        self.logger.debug('Read database: ' + self.record_type)
        db_host = self.config.get('crawled_database', 'db_host')
        db_port = int(self.config.get('crawled_database', 'db_port'))
        db_name = self.config.get('crawled_database', 'db_name')
        db_username = self.config.get('crawled_database', 'db_username')
        db_password = self.config.get('crawled_database', 'db_password')
        collection_name = self.config.get('crawled_database', 'collection_name')

        client = MongoClient(host=db_host, port=db_port, username=db_username, password=db_password)
        db = client[db_name]
        limit_query = int(self.config.get('crawled_database', 'limit_query'))
        self.collection = db[collection_name]

        if self.record_type == 'apartment':
            matching_condition = {
                "type": {"$exists" : True, "$regex": ".*sale_apartment.*"},
                "address_lat": {"$nin": [None, ""]},
                "address_long": {"$nin": [None, ""]},
                "start_date_datetime": {"$nin": [None, ""]}
            }
        elif self.record_type == 'house':
            matching_condition = {
                "type": {"$exists" : True, "$regex": ".*sale_house.*"},
                "address_lat": {"$nin": [None, ""]},
                "address_long": {"$nin": [None, ""]},
                "start_date_datetime": {"$nin": [None, ""]}
            }
        else:
            matching_condition = {
                "address_lat": {"$nin": [None, ""]},
                "address_long": {"$nin": [None, ""]},
                "start_date_datetime": {"$nin": [None, ""]}
            }

        aggregation_pipeline = [
            {"$match": matching_condition},
            {"$sort": {"start_date_datetime": -1}},
            {"$limit": limit_query}
        ]

        start = time.time()
        result = list(self.collection.aggregate(aggregation_pipeline, allowDiskUse=True))
        self.logger.debug("All aggregation records: " + str(len(result)))
        self.logger.debug("Aggregate time: " + str(time.time() - start))

        start = time.time()
        result = remove_dupplicate_records(result)
        self.result_without_price = []
        self.result = []
        for record in result:
            lat = record.get('address_lat', '')
            long = record.get('address_long', '')
            ppm2 = record.get('price_per_m2', -1)
            if lat > self.upper_right_limit[0] or lat < self.lower_left_limit[0] or \
                    long > self.upper_right_limit[1] or long < self.lower_left_limit[1]:
                continue
            if check_inside_geojson(self.limit_geojson, lat, long):
                if ppm2 < self.min_ppm2 or ppm2 > self.max_ppm2:
                    self.result_without_price.append(record)
                else:
                    self.result.append(record)
        self.logger.debug('Number of records without price: ' + str(len(self.result_without_price)))
        self.logger.debug('Number of queried records: ' + str(len(self.result)))
        self.logger.debug("Record filter: " + str(time.time() - start))

    def calculate_map_grid(self):
        start = time.time()
        # calculate grid geojson
        grids = get_geojson_grid(self.upper_right_limit, self.lower_left_limit, self.stride)
        self.map_grids = []

        # init map_grids store all MapGrid info
        for row_index, row_grid in enumerate(grids):
            column = []
            # iterate all grid from row to col and construct MapGrid for all grids, insert GridMap to self.map_grid by matrix
            for grid in row_grid:
                upper_right = grid["properties"]["upper_right"]
                lower_left = grid["properties"]["lower_left"]
                column.append(MapGrid(upper_right, lower_left, grid))
            self.map_grids.insert(row_index, column)

        # iterate all record and insert it to correspond grid
        for record in self.result:
            lat = record.get('address_lat')
            long = record.get('address_long')
            found_grid = find_container_grid(self.map_grids, lat, long)
            if found_grid != None:
                found_grid.add_record(record)

        # calculate ppm2 mean for all grid
        for map_grid in chain.from_iterable(self.map_grids):
            # map_grid.calculate_price_per_m2_mean()
            map_grid.reject_outliers_and_calculate_predicted_range()
            map_grid.update_predicted_range_for_inside_records()

        self.logger.debug('Calculate map grid: ' + str(time.time() - start))

    def update_map_grid(self, new_record):
        start = time.time()

        # iterate all record and insert it to correspond grid
        for record in new_record:
            lat = record.get('address_lat')
            long = record.get('address_long')
            found_grid = find_container_grid(self.map_grids, lat, long)
            if found_grid != None:
                found_grid.add_record(record)

        # calculate ppm2 mean for all grid
        for map_grid in chain.from_iterable(self.map_grids):
            # map_grid.calculate_price_per_m2_mean()
            map_grid.reject_outliers_and_calculate_predicted_range()
            map_grid.update_predicted_range_for_inside_records()

        self.logger.debug('Update map grid with ' + str(len(new_record)) + ' new records: ' + str(time.time() - start))

    def calculate_mean_price_empty_grid(self):
        """
        calculate ppm2 for empty grid (grid doesn't have any record) by calculate mean on 8 neighbor grid
        :return:
        """
        start = time.time()
        n_rows = len(self.map_grids)
        n_cols = len(self.map_grids[0])

        # find neighbors coords function
        neighbors = lambda x, y: [(x2, y2) for x2 in range(x - 1, x + 2)
                                  for y2 in range(y - 1, y + 2)
                                  if (-1 < x < n_rows and
                                      -1 < y < n_cols and
                                      (x != x2 or y != y2) and
                                      (0 <= x2 < n_rows) and
                                      (0 <= y2 < n_cols))]

        empty_grid_calculated = 0
        for row_index, row_grid in enumerate(self.map_grids):
            for col_index, grid in enumerate(row_grid):
                if len(grid.record_inside_box) < 100 :    # empty record
                    grid_neighbors = neighbors(row_index, col_index)
                    all_neighbor_record = []
                    for neighbor_index in grid_neighbors:
                        neighbor_grid = self.map_grids[neighbor_index[0]][neighbor_index[1]]
                        if len(neighbor_grid.record_inside_box) > 0:
                            all_neighbor_record.append(neighbor_grid.ppm2_mean_inside_box)
                    if len(all_neighbor_record) > 0:
                        grid.ppm2_mean_inside_box = mean(all_neighbor_record)
                        grid.reject_outliers_and_calculate_predicted_range()
                        empty_grid_calculated += 1
        self.logger.debug('Calculated ' + str(empty_grid_calculated) + ' empty grid: ' + str(time.time() - start))

    def predict_by_map_grid(self, record):
        """
        predict ppm2 for a record by finding its grid and assign its ppm2 = grid mean ppm2
        :param record:
        :return:
        """
        lat = record.get('address_lat')
        long = record.get('address_long')

        found_grid = find_container_grid(self.map_grids, lat, long)
        if found_grid != None:
            new_std = (2.0/3) * found_grid.std
            return np.random.normal(found_grid.ppm2_mean_inside_box, new_std, 1)[0]

        return -1

    def calculate_price_for_negotiate_price_record_by_map_grid(self):
        """
        calculate for all record have negotiate ppm2 (all record in self.result_without_price) using map grid
        :return:
        """
        start = time.time()
        negotiate_calculated = 0
        negotiate_calculated_result = []
        for record in self.result_without_price:
            ppm2_prediction_price = self.predict_by_map_grid(record)
            if ppm2_prediction_price > 0.0:
                record['price_per_m2'] = ppm2_prediction_price
                negotiate_calculated += 1
                negotiate_calculated_result.append(record)
        self.logger.debug('Calculated ' + str(negotiate_calculated) + ' negotiate records: ' + str(time.time() - start))
        return negotiate_calculated_result

    def visualize_map_grid(self):
        """
        Visualize map grid
        :return:
        """
        self.logger.debug('Visualize map grid')
        hanoi_lat = 21.028511
        hanoi_long = 105.804817
        self.folium_map = folium.Map(location=[hanoi_lat, hanoi_long], zoom_start=15)

        all_records = self.get_all_records()

        # draw marker and popup to map
        locations = [(record.get('address_lat'), record.get('address_long')) for record in all_records]
        if len(locations) == 0:
            return;
        popups = ["{:s} - {:s} - {:.2f} triệu/m²".format(record.get('type', ''), record.get('address'), record.get('price_per_m2')) for record in
                  all_records]
        icons = [folium.Icon(icon="home", prefix="fa") for _ in range(len(locations))]

        # MarkerCluster
        cluster = MarkerCluster(locations=locations, icons=icons, popups=popups)
        self.folium_map.add_child(cluster)

        # Calculate exposures in grid
        price_per_m2_all_records = []
        for map_grid in chain.from_iterable(self.map_grids):
            price_per_m2_all_records.append(map_grid.ppm2_mean_inside_box)

            # assign popup to box
            content = "Price per m2 mean: {:.2f} triệu/m²".format(map_grid.ppm2_mean_inside_box)
            popup = folium.Popup(content)
            map_grid.set_popup(popup)

        # find max box price
        # self.logger.debug(len(price_per_m2_all_records))
        # price_per_m2_all_records_remove_ourliers = reject_outliers(np.array(price_per_m2_all_records), m = 5)
        # self.logger.debug(len(price_per_m2_all_records_remove_ourliers))
        map_grid_max_price_mean = max(price_per_m2_all_records)
        self.logger.debug("MAX PRICE: " + str(map_grid_max_price_mean))
        for map_grid in chain.from_iterable(self.map_grids):
            map_grid.visualize_grid(self.folium_map, map_grid_max_price_mean)

        self.folium_map.save('map_grid.html')

    def update_grid_calculated_database(self):
        self.logger.debug('Update grid calculated database')
        db_host = self.config.get('grid_calculated_database', 'db_host')
        db_port = int(self.config.get('grid_calculated_database', 'db_port'))
        db_name = self.config.get('grid_calculated_database', 'db_name')
        db_username = self.config.get('grid_calculated_database', 'db_username')
        db_password = self.config.get('grid_calculated_database', 'db_password')
        if self.record_type == 'house':
            collection_name = self.config.get('grid_calculated_database', 'collection_house_name')
        elif self.record_type == 'apartment':
            collection_name = self.config.get('grid_calculated_database', 'collection_apartment_name')
        elif self.record_type == 'all':
            collection_name = self.config.get('grid_calculated_database', 'collection_all_name')

        client = MongoClient(host=db_host, port=db_port, username=db_username, password=db_password)
        db = client[db_name]

        all_records = self.get_all_records()
        if len(all_records) > 0:
            grid_calculated_collection = db[collection_name]
            grid_calculated_collection.delete_many({})
            grid_calculated_collection.insert_many(all_records)

        #update record in advert collection
        for record in all_records:
            self.collection.update_one({"_id": record.get('_id')}, {"$set": record})

    def update_map_grid_database(self):
        self.logger.debug('Update map grid database')
        db_host = self.config.get('map_grid_database', 'db_host')
        db_port = int(self.config.get('map_grid_database', 'db_port'))
        db_name = self.config.get('map_grid_database', 'db_name')
        db_username = self.config.get('grid_calculated_database', 'db_username')
        db_password = self.config.get('grid_calculated_database', 'db_password')
        if self.record_type == 'house':
            collection_name = self.config.get('map_grid_database', 'collection_house_grid')
        elif self.record_type == 'apartment':
            collection_name = self.config.get('map_grid_database', 'collection_apartment_grid')
        elif self.record_type == 'all':
            collection_name = self.config.get('map_grid_database', 'collection_all_grid')

        client = MongoClient(host=db_host, port=db_port, username=db_username, password=db_password)
        db = client[db_name]
        map_grid_collection = db[collection_name]

        all_map_grids = self.get_all_map_grid()
        if len(all_map_grids) > 0:
            map_grid_collection.delete_many({})
            map_grid_collection.insert_many(all_map_grids)

    def get_all_map_grid(self):
        result = []
        for map_grid in chain.from_iterable(self.map_grids):
            result.append(map_grid.get_map_grid_info())
        return result

    def get_all_records(self):
        result = []
        for map_grid in chain.from_iterable(self.map_grids):
            result += map_grid.record_inside_box
        return result

    def start_grid_calculation(self):
        self.logger.debug('-------------------------------------------------------------')
        self.logger.debug('-------------------------------------------------------------')
        self.logger.debug("It's time to start Grid Calculation for " + self.record_type + ": " + self.config.get('schedule', 'start_time'))

        self.read_database()     # read db

        # calculate map grid end empty grid
        self.calculate_map_grid()
        self.calculate_mean_price_empty_grid()

        # calculate negotiate price record by map grid
        records_negotiate_price_calculated = self.calculate_price_for_negotiate_price_record_by_map_grid()

        # recalculate negotiate price record by map grid
        self.update_map_grid(records_negotiate_price_calculated)
        self.calculate_mean_price_empty_grid()

        # update grid calculated to db
        self.update_grid_calculated_database()

        # upodate grid map to db
        self.update_map_grid_database()

        # main.visualize_map_grid()
        self.logger.debug('Done\n')

    def start_grid_calculation_schedule(self):
        self.logger.debug('Start Grid Calculation schedule')
        schedule.every().day.at(self.config.get('schedule', 'start_time')).do(self.start_grid_calculation)

        while True:
            try:
                schedule.run_pending()
                time.sleep(60)
            except:
                self.logger.error("Exception when scheduling")
                traceback.print_exc()

if __name__ == '__main__':
    grid_calculation = GridCalculation("config/config.cfg")
    grid_calculation.start_grid_calculation_schedule()
    # grid_calculation.start_grid_calculation()