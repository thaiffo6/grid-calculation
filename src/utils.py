import numpy as np
from datetime import datetime
from collections import namedtuple
from shapely.geometry import shape, Point
import os
import json
import random
import math

def get_geojson_grid(upper_right, lower_left, stride = 0.001):
    """Returns a grid of geojson rectangles, and computes the exposure in each section of the grid based on the vessel data.

    Parameters
    ----------
    upper_right: array_like
        The upper right hand corner of "grid of grids" (the default is the upper right hand [lat, lon] of the USA).

    lower_left: array_like
        The lower left hand corner of "grid of grids"  (the default is the lower left hand [lat, lon] of the USA).

    n: integer
        The number of rows/columns in the (n,n) grid.

    Returns
    -------

    list
        List of "geojson style" dictionary objects
    """

    all_boxes = []

    n = int((upper_right[1] - lower_left[1]) / stride)

    lat_steps = np.linspace(lower_left[0], upper_right[0], n+1)
    lon_steps = np.linspace(lower_left[1], upper_right[1], n+1)

    lat_stride = lat_steps[1] - lat_steps[0]
    lon_stride = lon_steps[1] - lon_steps[0]

    for row_index, lat in enumerate(lat_steps[:-1]):
        row = []
        for lon in lon_steps[:-1]:
            # Define dimensions of box in grid
            upper_left = [lon, lat + lat_stride]
            upper_right = [lon + lon_stride, lat + lat_stride]
            lower_right = [lon + lon_stride, lat]
            lower_left = [lon, lat]

            # Define json coordinates for polygon
            coordinates = [
                upper_left,
                upper_right,
                lower_right,
                lower_left,
                upper_left
            ]

            geo_json = {"type": "FeatureCollection",
                        "properties":{
                            "lower_left": lower_left,
                            "upper_right": upper_right
                        },
                        "features":[]}

            grid_feature = {
                "type":"Feature",
                "geometry":{
                    "type":"Polygon",
                    "coordinates": [coordinates],
                }
            }

            geo_json["features"].append(grid_feature)

            row.append(geo_json)

        all_boxes.insert(row_index, row)

    return all_boxes

def check_datetime_overlap(s1, e1, s2, e2):
    Range = namedtuple('Range', ['start', 'end'])
    r1 = Range(start=s1, end=e1)
    r2 = Range(start=s2, end=e2)
    latest_start = max(r1.start, r2.start)
    earliest_end = min(r1.end, r2.end)
    delta = (earliest_end - latest_start).days + 1
    overlap = max(0, delta)
    return overlap != 0

def check_inside_geojson(geojson, lat, long):
    point = Point(long, lat)
    polygon = shape(geojson)
    return polygon.contains(point)

def find_geojson_bounding_box(geojson):
    polygon = shape(geojson)
    return polygon.bounds

def separate_administrative_id(administrative_level, administrative_id):
    if administrative_level == 2:
        administrative_id = administrative_id.replace('_', '.')
        administrative_id_split = administrative_id.split('.')
        id0 = administrative_id_split[0]
        id1 = administrative_id_split[1]
        id2 = administrative_id_split[2]
        id3 = administrative_id_split[3]
        city_id = id0 + '.' + id1 + '_' + id3
        district_id = id0 + '.' + id1 + '.' + id2 + '_' + id3

        return city_id, district_id
    elif administrative_level == 3:
        administrative_id = administrative_id.replace('_', '.')
        administrative_id_split = administrative_id.split('.')
        id0 = administrative_id_split[0]
        id1 = administrative_id_split[1]
        id2 = administrative_id_split[2]
        id3 = administrative_id_split[3]
        id4 = administrative_id_split[4]
        city_id = id0 + '.' + id1 + '_' + id4
        district_id = id0 + '.' + id1 + '.' + id2 + '_' + id4
        ward_id = id0 + '.' + id1 + '.' + id2 + '.' + id3 + '_' + id4

        return city_id, district_id, ward_id

def find_geojson_limit(geojson_data, limit_level, limit_id):
    if limit_level == 0:
        return geojson_data.get('features')[0].get('geometry')
    elif limit_level == 1:
        for city in geojson_data.get('cities'):
            if city.get('properties').get('GID_1') == limit_id:
                return city.get('geometry')
    elif limit_level == 2:
        city_id, district_id = separate_administrative_id(limit_level, limit_id)
        for city in geojson_data.get('cities'):
            if city.get('properties').get('GID_1') == city_id:
                for district in city.get('districts'):
                    if district.get('properties').get('GID_2') == district_id:
                        return district.get('geometry')
    elif limit_level == 3:
        city_id, district_id, ward_id = separate_administrative_id(limit_level, limit_id)
        for city in geojson_data.get('cities'):
            if city.get('properties').get('GID_1') == city_id:
                for district in city.get('districts'):
                    if district.get('properties').get('GID_2') == district_id:
                        for ward in district.get('wards'):
                            if ward.get('properties').get('GID_3') == ward_id:
                                return ward.get('geometry')

def remove_dupplicate_records(new_data):
    # remove dupplicate inside new_data
    seen = set()
    new_l = []
    for record in new_data:
        t = tuple(record.get('url'))
        if t not in seen:
            seen.add(t)
            new_l.append(record)
    return new_l

def find_container_grid(map_grids, lat, long):
    lat_stride = map_grids[1][0].lower_left[1] - map_grids[0][0].lower_left[1]
    long_stride = map_grids[0][1].lower_left[0] - map_grids[0][0].lower_left[0]

    min_long = map_grids[0][0].lower_left[0]
    min_lat = map_grids[0][0].lower_left[1]

    grid_row = int((lat - min_lat) // lat_stride)
    grid_col = int((long - min_long) // long_stride)

    found_grid = map_grids[grid_row][grid_col]
    if found_grid.check_inside_grid(lat, long):
        return found_grid
    else:
        return None