from statistics import mean
import json
from matplotlib import pyplot as plt
import matplotlib as mpl
import numpy as np
import folium
import math

class MapGrid:
    def __init__(self, upper_right, lower_left, geojson):
        self.upper_right = upper_right
        self.lower_left = lower_left
        self.record_inside_box = []
        self.geojson = geojson
        self.under1 = 0.0
        self.under2 = 0.0
        self.over1 = 0.0
        self.over2 = 0.0
        self.min = 0.0
        self.max = 0.0
        self.ppm2_mean_inside_box = 0.0

    def add_record(self, record):
        self.record_inside_box.append(record)

    def set_popup(self, popup):
        self.popup = popup

    def visualize_grid(self, m, map_grid_max_price_mean):
        geo_json = json.dumps(self.geojson)

        color = plt.cm.Reds(self.ppm2_mean_inside_box / map_grid_max_price_mean)
        color = mpl.colors.to_hex(color)

        gj = folium.GeoJson(geo_json,
                            style_function=lambda feature, color=color: {
                                                                            'stroke': False,
                                                                            'fillColor': color,
                                                                            'color':"black",
                                                                            'weight': 0.5,
                                                                            'dashArray': '5, 5',
                                                                            'fillOpacity': 0.7,
                                                                        })

        gj.add_child(self.popup)
        m.add_child(gj)

    def check_inside_grid(self, lat, long):
        if lat <= self.upper_right[1] and lat >= self.lower_left[1] and long <= self.upper_right[0] and long >= self.lower_left[0]:
            return True
        return False

    def update_predicted_range_for_inside_records(self):
        for record in self.record_inside_box:
            record['under2'] = self.under2
            record['under1'] = self.under1
            record['mean'] = self.ppm2_mean_inside_box
            record['over1'] = self.over1
            record['over2'] = self.over2

    def reject_outliers_and_calculate_predicted_range(self):
        """
        reject outliers
        calculate min, max, mean price
        calculate predicted price range (-2sigma, -1sigma, mean, 1sigma, 2sigma)
        :return:
        """
        if len(self.record_inside_box) > 0:
            all_ppm2 = self.reject_outliers()
            self.min = min(all_ppm2)
            self.max = max(all_ppm2)

            self.std = np.std(all_ppm2)
            self.ppm2_mean_inside_box = np.mean(all_ppm2)
            if math.fabs(self.std) < 5:
                self.std = 5
            if self.ppm2_mean_inside_box - 2 * self.std <= 0:
                self.std = self.ppm2_mean_inside_box / 3

            self.under1 = self.ppm2_mean_inside_box - self.std
            self.under2 = self.ppm2_mean_inside_box - 2 * self.std
            self.over1 = self.ppm2_mean_inside_box + self.std
            self.over2 = self.ppm2_mean_inside_box + 2 * self.std
        else:
            self.std = 5
            if self.ppm2_mean_inside_box - 2 * self.std <= 0:
                self.std = self.ppm2_mean_inside_box / 3

            self.under1 = self.ppm2_mean_inside_box - self.std
            self.under2 = self.ppm2_mean_inside_box - 2 * self.std
            self.over1 = self.ppm2_mean_inside_box + self.std
            self.over2 = self.ppm2_mean_inside_box + 2 * self.std

    def reject_outliers(self, threshold=3):
        """
        reject outliers by assign outliers value by median of data
        :param threshold:
        :return:
        """
        data = [record.get('price_per_m2') for record in self.record_inside_box]
        data_median = np.median(data)
        data = np.asarray(data).copy()
        difference = np.abs(data - data_median)
        median_difference = np.median(difference)
        if median_difference == 0:
            s = np.ndarray(shape=difference.shape)
        else:
            s = difference / float(median_difference)
        mask = s > threshold
        for i, record in enumerate(self.record_inside_box):
            if mask[i] == True:
                record['price_per_m2'] = data_median
        return [record.get('price_per_m2') for record in self.record_inside_box]

    def get_map_grid_info(self):
        return {
            'upper_right': self.upper_right,
            'lower_left': self.lower_left,
            'under2': self.under2,
            'under1': self.under1,
            'mean': self.ppm2_mean_inside_box,
            'over1': self.over1,
            'over2': self.over2
        }